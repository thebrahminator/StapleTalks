from flask import Flask, url_for, render_template, request, session, flash, send_file, redirect
from models import db, Admin, Event, Participant, Description
import uuid
import re
import csv
import os
from datetime import datetime
app = Flask(__name__, static_url_path="")
app.secret_key="JinjaMaster"

app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://thebrahminator:beyblade@localhost:5432/stabletalk'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, "static/files")
app.config["UPLOAD_FOLDER"]= UPLOAD_FOLDER
db.init_app(app)

'''
Home Page, here's where Shruthi's Front End must go.
'''
@app.route('/')
def hello_world():
    eventDetails = Event.query.filter_by(status=0).first()

    placeholder = "Not Added"
    if not eventDetails:
        time = placeholder
        date = placeholder
        place = placeholder
    else:
        time = eventDetails.time
        date = eventDetails.date_of_event
        place = eventDetails.address

    return render_template('frontendnew/index.html', time=time, date=date, place=place)

'''
Login module starts
'''
@app.route('/getLogin', methods=["GET"])
def getLogin():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            return redirect(url_for('dashboard'))
        else:
            return render_template('auth/login.html')
    else:
        return render_template('auth/login.html')


@app.route('/postLogin', methods=["POST"])
def postLogin():
    username = request.form.get('username')
    password = request.form.get('password')

    admins = Admin.query.all()
    print(username)
    print(admins)
    for admin in admins:
        print(admin.uname)
        if admin.uname == username:
            print("1")
            print(admin.uname)
            if admin.password == password:
                session['loggedIn'] = True
                session['userid'] = admin.id
                session['name'] = admin.name
                return redirect(url_for('dashboard'))
            else:
                flash("Wrong Password")
                return redirect(url_for('getLogin'))

    return redirect(url_for('getLogin'))
'''
Login module Ends
'''

'''
Forgot Password Module Starts
'''

@app.route('/forgotPassword', methods=["GET"])
def forgotPassword():
    return render_template('auth/forgotpassword.html')

@app.route('/postForgotPassword', methods=["POST"])
def postForgotPassword():
    username = request.form.get('username', '')
    password = request.form.get('password', '')

    admins = Admin.query.all()

    for admin in admins:
        if username == admin.uname:
            admin.password = password
            db.session.commit()
            flash("Changed password")
            return redirect(url_for('getLogin'))

    flash("Username not found, please register")
    return render_template('auth/forgotpassword.html')

'''Forgot Password Module Ends'''

#LOGOUT MODULE
@app.route('/logout', methods=["GET"])
def adminLogout():
    session['loggedIn'] = False
    return redirect(url_for('getLogin'))

'''Admin Registration Starts'''
@app.route('/adminRegister', methods=["GET"])
def getAdminRegister():
    return render_template('auth/register.html')

@app.route('/postAdmin', methods=["POST"])
def postAdminRegister():
    id = str(uuid.uuid1())
    uname = request.form.get('username', '')
    name = request.form.get('name', '')
    password = request.form.get('password', '')
    email = request.form.get('email', '')
    confirm = Admin(id=id, uname=uname, name=name, password=password, email=email)
    db.session.add(confirm)
    db.session.commit()
    flash("Successfully added user")
    return redirect(url_for('getLogin'))

'''Admin Registration Ends'''

@app.route('/dashboard', methods=["GET"])
def dashboard():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            desc = Description.query.all()
            if not desc:
                description = "Nothing yet"
            else:
                description=desc[0].desc
            #print(desc[0].desc)
            return render_template('dashboard/homepage.html', description=description)
        else:
            return redirect(url_for('getLogin'))
    else:
        return redirect(url_for('getLogin'))

#psql -h localhost -p 5432 -U postgress testdb
'''Downloading CSV, for the data which has been stored'''
#TODO convert it from admin to participant registration module. -- done
@app.route('/download', methods=["GET"])
def downloadCSV():
    admins = Admin.query.all()
    rows = []
    for data in admins:
        lol = {
            'id': data.id,
            'uname': data.uname,
            'name': data.name,
            'password': data.password,
            'email': data.email
        }

        rows.append(lol)
    keys = ["id", "uname", "name", "password", "email"]
    file_name = datetime.now().strftime("%d-%m-%Y%l:%M%p")
    file_name = str(file_name)
    #file_time = datetime.now().time()
    #print(file_time)
    print(file_name)
    file_name = file_name + ".csv"
    with open(os.path.join(app.config["UPLOAD_FOLDER"], file_name), "w") as csvbridge:
        dtwriter = csv.DictWriter(csvbridge,keys)
        dtwriter.writeheader()
        print(rows)
        dtwriter.writerows(rows)

    return send_file(os.path.join(app.config["UPLOAD_FOLDER"], file_name), as_attachment=True)

@app.route('/addDescription', methods=["POST"])
def addDesc():
    desc = request.form.get('description', '')
    print(desc)
    no_of_rows = Description.query.delete()
    print(no_of_rows)
    identifier = str(uuid.uuid1())
    descs = Description(identifier,desc)
    db.session.add(descs)
    db.session.commit()
    flash("Changed Description")
    return redirect(url_for('dashboard'))

'''Registration of Talk Starts
'''
#TODO: Integrate this with index.html file, then only it'll be complete. -- done
@app.route('/register', methods=["POST"])
def RegisterForTalk():
    age = 0
    userid = str(uuid.uuid1())
    name = request.form.get('name', '')
    email = request.form.get('email', '')
    mobile = request.form.get('contact', '')
    try:
        age = int(request.form.get('age', ''))
    except:
        pass
    profession = request.form.get('des', '')
    plusone = request.form.get('plusone', '')
    regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

    print(userid)
    #participant = Participant(id=id, name=name, email=email, mobile=mobile, age=age, profession=profession)
    eventid = Event.query.filter_by(status=0).first()
    participant = Participant(id=userid, name=name, age=age, email=email, mobile=mobile,
                              profession=profession, eventid=eventid.id, plusone=plusone)
    db.session.add(participant)
    db.session.commit()

    flash("You've been successfully Registered")
    return redirect(url_for('hello_world'))

#Temporary Registration File
@app.route('/tempreg', methods=["GET"])
def tempreg():
    return render_template('tempreg.html')


@app.route('/regEvent', methods=["POST"])
def registerEvent():

    eventid = str(uuid.uuid1())
    venue = request.form.get('address', '')
    date = request.form.get('date', '')
    time = request.form.get('time', '')

    events = Event.query.all()
    for event in events:
        event.status = 1
        db.session.commit()
    event = Event(time=time, date=date, address=venue, id=eventid, status=0)

    db.session.add(event)
    db.session.commit()

    flash("Event added to the list")
    return redirect(url_for('dashboard'))

@app.route('/displayEvent', methods=["GET"])
def DisplayEvent():
    events = Event.query.all()
    reversed(events)
    rev_events = []
    for event in events:
        rev_events.insert(0,event)
    return render_template('dashboard/allevents.html', events=rev_events)

@app.route('/allParticipant', methods=["GET"])
def allParticipant():
    eventid = request.args.get('eventid', '')
    participants = Participant.query.filter_by(eventid=eventid).all()
    reversed(participants)
    listofparticipants = []
    for participant in participants:
        listofparticipants.insert(0,participant)

    print(listofparticipants)
    return render_template('dashboard/allpariticipants.html', participants=listofparticipants)


@app.route('/downloadParticipant', methods=["GET"])
def downloadParticipant():
    eventid = request.args.get('eventid', '')
    participants = Participant.query.filter_by(eventid=eventid).all()
    rows = []
    for data in participants:
        lol = {
            'name': data.name,
            'age': data.age,
            'email': data.email,
            'mobileno': data.mobileno,
            'profession': data.prof,
            'plusone': data.plusone
        }

        rows.append(lol)
    keys = ["name", "age", "email", "mobileno", "profession", "plusone"]
    file_name = datetime.now().strftime("%d-%m-%Y%l:%M%p")
    file_name = str(file_name)
    # file_time = datetime.now().time()
    # print(file_time)
    print(file_name)
    file_name = file_name + ".csv"
    with open(os.path.join(app.config["UPLOAD_FOLDER"], file_name), "w") as csvbridge:
        dtwriter = csv.DictWriter(csvbridge, keys)
        dtwriter.writeheader()
        print(rows)
        dtwriter.writerows(rows)

    return send_file(os.path.join(app.config["UPLOAD_FOLDER"], file_name), as_attachment=True)

@app.route('/editEvent', methods=["GET"])
def editEvent():
    eventid = request.args.get('id', '')
    events = Event.query.filter_by(id=eventid).all()
    sent_event = []
    for event in events:
        sent_event = event
    return render_template('dashboard/editevent.html', event=sent_event)

@app.route('/postEditEvent', methods=["POST"])
def postEditEvent():
    eventid = request.form.get('id', '')
    venue = request.form.get('address', '')
    date = request.form.get('date', '')
    time = request.form.get('time', '')

    events = Event.query.filter_by(id=eventid).all()

    for event in events:
        event.address = venue
        event.date_of_event = date
        event.time = time
        db.session.commit()
    flash("Details updated")
    return redirect(url_for('DisplayEvent'))

if __name__ == '__main__':
    app.run(debug=True, port=5001, host="0.0.0.0")
