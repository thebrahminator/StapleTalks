import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()

class Participant(db.Model):

    __tablename__ = "participant"

    id = db.Column(db.String(128), primary_key=True)
    name = db.Column(db.String(128))
    age = db.Column(db.Integer)
    email = db.Column(db.VARCHAR(128))
    mobileno = db.Column(db.String(128))
    prof = db.Column(db.VARCHAR(128))
    eventid = db.Column(db.String(128))
    plusone = db.Column(db.String(128))
    def __init__(self, id, name, age, email, mobile, profession, eventid, plusone):
        self.id = id
        self.age = age
        self.name = name
        self.mobileno = mobile
        self.email = email
        self.prof = profession
        self.eventid = eventid
        self.plusone = plusone

class Description(db.Model):
    __tablename__ = "description"

    desc = db.Column(db.String(1024), primary_key=True)
    original_desc = db.Column(db.VARCHAR(1024))
    def __init__(self, description,orginal_desc):
        self.desc = description
        self.original_desc=orginal_desc

class Admin(db.Model):

    __tablename__ = "admin"

    id = db.Column(db.String(128), primary_key=True)
    uname = db.Column(db.String(128))
    name = db.Column(db.String(128))
    password = db.Column(db.String(128))
    email = db.Column(db.String(128))

    def __init__(self, id, uname, name, password, email):
        self.id = id
        self.email = email
        self.name = name
        self.uname = uname
        self.password = password


class Event(db.Model):

    __tablename__ = "event"

    id = db.Column(db.String(128), primary_key=True)
    time = db.Column(db.String(128))
    address = db.Column(db.String(128))
    date_of_event = db.Column(db.String(128))
    status = db.Column(db.Integer)

    def __init__(self, time, address, date, id, status):
        self.id = id
        self.time = time
        self.address = address
        self.date_of_event = date
        self.status = status
